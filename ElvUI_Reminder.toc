## Interface: 50400
## Title: |cff1784d1ElvUI |rReminder
## Author: Sortokk
## Version: 1.15
## Notes: Show warning icons on your screen when you are missing buffs or have buffs when you shouldn't.
## OptionalDeps: ElvUI, Enhanced_Config

locales\load_locales.xml
load_reminder.xml

